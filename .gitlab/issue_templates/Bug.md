[Bug description]

**Current behaviour:**

[Explain current behaviour]

**Expected behaviour:**

[Explain expected behaviour]

**Criticality:** [1-10]

**Steps to reproduce:**