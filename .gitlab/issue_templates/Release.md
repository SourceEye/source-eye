[Release description]

### Steps to follow:
* Reintegrate develop branch into master (rebase if necessary)
* Create the release with ```./gradlew final```
* Check that everything went OK
* Create and upload the docker image to the registry with: ```./gradlew dockerRelease -Ptag <version>```
